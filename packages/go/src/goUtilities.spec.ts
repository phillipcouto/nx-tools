import { GoWorkFile, parseGoWorkFile, writeGoWorkFile } from './goUtilities';
import { EOL } from 'os';

describe('parseGoWorkFile', () => {
  it('should error with an empty or undefined input', async () => {
    await expect(async () => await parseGoWorkFile(null)).rejects.toThrow(
      'input is empty'
    );
    await expect(async () => await parseGoWorkFile(undefined)).rejects.toThrow(
      'input is empty'
    );
    await expect(async () => await parseGoWorkFile('')).rejects.toThrow(
      'input is empty'
    );
    await expect(
      async () => await parseGoWorkFile(Buffer.alloc(0))
    ).rejects.toThrow('input is empty');
  });

  it('should handle just the go version', async () => {
    const input = 'go 1.19';

    const result = await parseGoWorkFile(input);
    expect(result).toBeDefined();
    expect(result.go).toBe('1.19');
  });

  it('should parse all directives', async () => {
    const input = [
      'go 1.19',
      '',
      'toolchain go1.19.1',
      '',
      'use ./module/a',
      '',
      'replace golang.org/x/net v1.2.3 => example.com/fork/net v1.4.5',
    ].join('\n');

    const result = await parseGoWorkFile(input);
    expect(result.go).toBe('1.19');
    expect(result.toolchain).toBe('go1.19.1');
    expect(result.use).toHaveLength(1);
    expect(result.use[0]).toBe('./module/a');
    expect(result.replace).toHaveLength(1);
    expect(result.replace[0]).toBe(
      'golang.org/x/net v1.2.3 => example.com/fork/net v1.4.5'
    );
  });

  it('should handle a mix of use directives', async () => {
    const input = [
      'go 1.19',
      'use ./module/abc',
      'use (',
      '\t./module/def',
      '\t./module/ghi',
      ')',
    ].join('\n');
    const result = await parseGoWorkFile(input);
    expect(result.use).toHaveLength(3);
    expect(result.use).toEqual([
      './module/abc',
      './module/def',
      './module/ghi',
    ]);
  });

  it('should handle a mix of replace directives', async () => {
    const input = [
      'go 1.19',
      'replace github.com/bad/a v1.0.0 => github.com/replace/a v1.0.0',
      'replace (',
      '\tgithub.com/bad/b v1.0.0 => github.com/replace/b v1.0.0',
      '\tgithub.com/bad/c v1.0.0 => github.com/replace/c v1.0.0',
      ')',
    ].join('\n');
    const result = await parseGoWorkFile(input);
    expect(result.replace).toHaveLength(3);
    expect(result.replace).toEqual([
      'github.com/bad/a v1.0.0 => github.com/replace/a v1.0.0',
      'github.com/bad/b v1.0.0 => github.com/replace/b v1.0.0',
      'github.com/bad/c v1.0.0 => github.com/replace/c v1.0.0',
    ]);
  });

  it('should error when a multiline use directive is not complete', async () => {
    const input = ['go 1.19', 'use (', '\t./module/def', '\t./module/ghi'].join(
      '\n'
    );
    await expect(async () => await parseGoWorkFile(input)).rejects.toThrow(
      `invalid file, multiline directive 'use' not complete`
    );
  });

  it('should error when a multiline replace directive is not complete', async () => {
    const input = [
      'go 1.19',
      'replace (',
      '\tgithub.com/bad/b v1.0.0 => github.com/replace/b v1.0.0',
      '\tgithub.com/bad/c v1.0.0 => github.com/replace/c v1.0.0',
    ].join('\n');
    await expect(async () => await parseGoWorkFile(input)).rejects.toThrow(
      `invalid file, multiline directive 'replace' not complete`
    );
  });

  it('should error when the go directive is not defined', async () => {
    const input = [
      'toolchain go1.19.1',
      'use ./module/a',
      'replace golang.org/x/net v1.2.3 => example.com/fork/net v1.4.5',
    ].join('\n');

    await expect(async () => await parseGoWorkFile(input)).rejects.toThrow(
      'invalid file, go directive missing'
    );
  });
});

describe('writeGoWorkFile', () => {
  it('should only render defined directives', () => {
    const result = writeGoWorkFile({ go: '1.19', use: [], replace: [] });
    expect(result).toBe('go 1.19');
  });
  it('should render all directives', () => {
    const input: GoWorkFile = {
      go: '1.19',
      toolchain: 'go1.19.5',
      use: ['./module/a'],
      replace: ['github.com/bad/a v1.0.0 => github.com/good/a v1.0.0'],
    };

    const result = writeGoWorkFile(input);
    expect(result).toBe(
      [
        `go ${input.go}`,
        `toolchain ${input.toolchain}`,
        `use ${input.use[0]}`,
        `replace ${input.replace[0]}`,
      ].join(EOL + EOL)
    );
  });

  it('should render multiline use directive', () => {
    const input: GoWorkFile = {
      go: '1.19',
      use: ['./module/a', './module/b'],
      replace: [],
    };

    const useExpect = [
      'use (',
      ...input.use.map((value) => '\t' + value),
      ')',
    ].join(EOL);
    const expectedValue = 'go ' + input.go + EOL + EOL + useExpect;

    const result = writeGoWorkFile(input);
    expect(result).toBe(expectedValue);
  });

  it('should render multiline replace directive', () => {
    const input: GoWorkFile = {
      go: '1.19',
      use: [],
      replace: [
        'github.com/bad/a v1.0.0 => github.com/good/a v1.0.0',
        'github.com/bad/b v1.0.0 => github.com/good/b v1.0.0',
      ],
    };

    const replaceExpect = [
      'replace (',
      ...input.replace.map((value) => '\t' + value),
      ')',
    ].join(EOL);
    const expectedValue = 'go ' + input.go + EOL + EOL + replaceExpect;

    const result = writeGoWorkFile(input);
    expect(result).toBe(expectedValue);
  });
});
