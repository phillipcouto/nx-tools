import { formatFiles, generateFiles, Tree } from '@nx/devkit';
import { InitGeneratorSchema } from './schema';
import * as path from 'path';
import { getGoVersionFromCommand } from '../../goUtilities';

export async function initGenerator(tree: Tree, options: InitGeneratorSchema) {
  if (tree.exists('go.work')) {
    return;
  }

  let { version } = options;
  if (!version) {
    version = await getGoVersionFromCommand();
  }
  generateFiles(tree, path.join(__dirname, 'files'), '.', { version });
  await formatFiles(tree);
}

export default initGenerator;
