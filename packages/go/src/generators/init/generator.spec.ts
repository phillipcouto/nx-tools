import { createTreeWithEmptyWorkspace } from '@nx/devkit/testing';
import { Tree } from '@nx/devkit';

import { initGenerator } from './generator';
import { InitGeneratorSchema } from './schema';
import { getGoVersionFromCommand } from '../../goUtilities';

describe('init generator', () => {
  let tree: Tree;
  const options: InitGeneratorSchema = { version: '1.19' };

  beforeEach(() => {
    tree = createTreeWithEmptyWorkspace();
  });

  it('should create go.work', async () => {
    await initGenerator(tree, options);
    expect(tree.exists('go.work')).toEqual(true);

    const goWorkFile = tree.read('go.work').toString();
    expect(goWorkFile).toEqual('go 1.19');
  });

  it('should leave existing go.work files alone', async () => {
    tree.write('go.work', 'go 1.18');
    await initGenerator(tree, options);

    const goWorkFile = tree.read('go.work').toString();
    expect(goWorkFile).toEqual('go 1.18');
  });

  it('should use the version from go version if none provided', async () => {
    const currentGoVersion = await getGoVersionFromCommand();

    await initGenerator(tree, {});
    const goWorkFile = tree.read('go.work').toString();
    expect(goWorkFile).toEqual(`go ${currentGoVersion}`);
  });
});
