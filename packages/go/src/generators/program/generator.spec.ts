import { createTreeWithEmptyWorkspace } from '@nx/devkit/testing';
import { Tree, readProjectConfiguration, workspaceLayout } from '@nx/devkit';

import { programGenerator } from './generator';
import { ProgramGeneratorSchema } from './schema';
import * as path from 'path';
import { parseGoWorkFile } from '../../goUtilities';

describe('program generator', () => {
  let tree: Tree;
  const options: ProgramGeneratorSchema = { name: 'test' };

  beforeEach(() => {
    tree = createTreeWithEmptyWorkspace();
  });

  it('should create the project with the correct files', async () => {
    await programGenerator(tree, options);
    const config = readProjectConfiguration(tree, 'test');
    expect(config).toBeDefined();

    const { appsDir } = workspaceLayout();
    expect(tree.exists(path.join(appsDir, 'test', 'go.mod'))).toBeTruthy();
    expect(tree.exists(path.join(appsDir, 'test', 'main.go'))).toBeTruthy();
  });

  it('should create a project with a path as a name', async () => {
    await programGenerator(tree, { name: 'hello/world' });
    const config = readProjectConfiguration(tree, 'hello-world');
    expect(config).toBeDefined();

    const { appsDir } = workspaceLayout();
    expect(
      tree.exists(path.join(appsDir, 'hello', 'world', 'go.mod'))
    ).toBeTruthy();
    expect(
      tree.exists(path.join(appsDir, 'hello', 'world', 'main.go'))
    ).toBeTruthy();

    const goWorkFile = await parseGoWorkFile(tree.read('go.work'));
    expect(goWorkFile.use.includes('./' + path.join('hello', 'world')));
  });

  it('should create a project with the correct targets', async () => {
    await programGenerator(tree, options);
    const config = readProjectConfiguration(tree, 'test');
    const buildTarget = config.targets.build;
    expect(buildTarget).toBeDefined();
    expect(buildTarget.executor).toEqual('nx-tools-go:build');
    expect(buildTarget.options.output).toEqual('dist/apps/test/test');
    expect(buildTarget.configurations.development).toBeDefined();
    expect(buildTarget.configurations.development.race).toEqual(true);
    expect(buildTarget.configurations.production.trimPath).toEqual(true);
    expect(buildTarget.defaultConfiguration).toEqual('development');
  });
});
