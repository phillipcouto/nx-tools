import {
  addProjectConfiguration,
  formatFiles,
  generateFiles,
  Tree,
  workspaceLayout,
} from '@nx/devkit';
import * as path from 'path';
import { ProgramGeneratorSchema } from './schema';
import { parseGoWorkFile, writeGoWorkFile } from '../../goUtilities';
import initGenerator from '../init/generator';
import { nameWithPath, normalizeProjectName } from '../../utilities';

export async function programGenerator(
  tree: Tree,
  options: ProgramGeneratorSchema
) {
  const { appsDir } = workspaceLayout();
  const { name, path: pathFromOptions } = nameWithPath(options.name);

  const projectRoot = path.join(appsDir, pathFromOptions, name);
  const projectName = normalizeProjectName(options.name);
  addProjectConfiguration(tree, projectName, {
    root: projectRoot,
    projectType: 'application',
    sourceRoot: `${projectRoot}`,
    targets: {
      build: {
        executor: 'nx-tools-go:build',
        options: {
          output: ['dist', projectRoot, projectName].join('/'),
        },
        configurations: {
          development: {
            race: true,
          },
          production: {
            trimPath: true,
          },
        },
        defaultConfiguration: 'development',
      },
    },
  });
  await initGenerator(tree, {});

  // Parse go.work and add the new project to the `use` directive
  const goWorkFile = await parseGoWorkFile(tree.read('go.work'));
  goWorkFile.use.push('./' + projectRoot);
  tree.write('go.work', writeGoWorkFile(goWorkFile));

  // Initialize Mod file
  generateFiles(tree, path.join(__dirname, 'files'), projectRoot, {
    name,
    version: goWorkFile.go,
  });
  await formatFiles(tree);
}

export default programGenerator;
