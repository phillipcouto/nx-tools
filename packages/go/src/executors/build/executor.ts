import { BuildExecutorSchema } from './schema';
import { ExecutorContext } from 'nx/src/config/misc-interfaces';
import { executeGoCommand } from '../../goUtilities';
export default async function runExecutor(
  options: BuildExecutorSchema,
  context: ExecutorContext
) {
  let args = ['-o', options.outPath];

  if (context.isVerbose) {
    args.push('-v');
  }
  if (options.race) {
    args.push('-race');
  }
  if (options.tags) {
    args.push('-tags ' + options.tags.join(','));
  }
  if (options.trimPath) {
    args.push('-trimpath');
  }
  if (options.flags) {
    args = args.concat(options.flags);
  }

  let main =
    options.main ?? context.workspace.projects[context.projectName].root;
  if (!main.startsWith('./')) {
    main = './' + main;
  }
  args.push(main);
  executeGoCommand({ tool: 'build', args, env: options.env });
  return {
    success: true,
  };
}
