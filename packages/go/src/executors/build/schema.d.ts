export interface BuildExecutorSchema {
  outPath: string;
  main?: string;
  trimPath?: boolean;
  tags?: string[];
  race?: boolean;
  env?: { [key: string]: string };
  flags?: string[];
}
