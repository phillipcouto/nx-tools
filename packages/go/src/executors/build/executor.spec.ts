import { BuildExecutorSchema } from './schema';
import executor from './executor';
import { ExecutorContext } from 'nx/src/config/misc-interfaces';
import { execSync } from 'child_process';

jest.mock('child_process');

const mockExecSync = execSync as jest.MockedFn<typeof execSync>;

describe('Build Executor', () => {
  it('should create the correct command string', async () => {
    const exampleContext = {
      root: '',
      cwd: '',
      isVerbose: true,
      projectName: 'test',
      workspace: {
        version: 2,
        projects: {
          test: { root: 'packages/test' },
        },
      },
    } as ExecutorContext;

    const options: BuildExecutorSchema = {
      outPath: 'dist/test/test',
      main: 'packages/test/main.go',
      race: true,
      trimPath: true,
      tags: ['test', 'tags'],
      flags: ['-ldflags="-s -w"'],
      env: {
        GOOS: 'linux',
        GOARCH: 'amd64',
      },
    };

    mockExecSync.mockImplementation((command, options) => {
      expect(command).toEqual(
        'go build -o dist/test/test -v -race -tags test,tags -trimpath -ldflags="-s -w" ./packages/test/main.go'
      );
      expect(options.env.GOOS).toEqual('linux');
      expect(options.env.GOARCH).toEqual('amd64');
      return 'success';
    });

    const output = await executor(options, exampleContext);
    expect(output.success).toBe(true);
  });
});
