export interface TestExecutorSchema {
  packageList?: boolean;
  args?: string[];
  env?: { [key: string]: string };
} // eslint-disable-line
