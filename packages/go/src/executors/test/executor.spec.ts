import executor from './executor';
import { executeGoCommand, GoCommandInput } from '../../goUtilities';
import { ExecutorContext } from 'nx/src/config/misc-interfaces';
jest.mock('../../goUtilities');

const mockExecuteGoCommand = executeGoCommand as jest.MockedFn<
  typeof executeGoCommand
>;

describe('Test Executor', () => {
  it('should create the correct go command input', async () => {
    const exampleContext = {
      root: '',
      cwd: '',
      isVerbose: true,
      projectName: 'test',
      workspace: {
        version: 2,
        projects: {
          test: { root: 'packages/test' },
        },
      },
    } as ExecutorContext;
    mockExecuteGoCommand.mockImplementation((input: GoCommandInput) => {
      expect(input.tool).toBe('test');
      expect(input.args).toStrictEqual(['-v', './packages/test/...']);
      expect(input.env.GOOS).toEqual('linux');
      expect(input.env.GOARCH).toEqual('amd64');
    });

    const output = await executor(
      {
        packageList: true,
        env: {
          GOOS: 'linux',
          GOARCH: 'amd64',
        },
      },
      exampleContext
    );
    expect(output.success).toBe(true);
  });
});
