import { TestExecutorSchema } from './schema';
import { ExecutorContext } from 'nx/src/config/misc-interfaces';
import { executeGoCommand } from '../../goUtilities';

export default async function runExecutor(
  options: TestExecutorSchema,
  context: ExecutorContext
) {
  let packagePath = context.workspace.projects[context.projectName].root;
  if (!packagePath.startsWith('./')) {
    packagePath = './' + packagePath;
  }
  if (options.packageList) {
    packagePath += packagePath.endsWith('/') ? '...' : '/...';
  }
  const args = options.args ?? [];
  if (context.isVerbose) {
    args.push('-v');
  }
  args.push(packagePath);

  executeGoCommand({
    tool: 'test',
    args,
    env: options.env,
  });
  return {
    success: true,
  };
}
