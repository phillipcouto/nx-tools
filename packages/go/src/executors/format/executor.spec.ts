import executor from './executor';
import { ExecutorContext } from 'nx/src/config/misc-interfaces';
import * as gutils from '../../goUtilities';

describe('Format Executor', () => {
  it('should create the correct go command input', async () => {
    const exampleContext = {
      root: '',
      cwd: '',
      isVerbose: true,
      projectName: 'test',
      workspace: {
        version: 2,
        projects: {
          test: { root: 'packages/test' },
        },
      },
    } as ExecutorContext;
    jest
      .spyOn(gutils, 'executeGoCommand')
      .mockImplementation((input: gutils.GoCommandInput) => {
        expect(input.tool).toBe('fmt');
        expect(input.args).toStrictEqual(['./packages/test/...']);
      });

    const output = await executor(
      {
        packageList: true,
      },
      exampleContext
    );
    expect(output.success).toBe(true);
  });
});
