import { FormatExecutorSchema } from './schema';
import { ExecutorContext } from 'nx/src/config/misc-interfaces';
import { executeGoCommand, getPackagePath } from '../../goUtilities';

export default async function runExecutor(
  options: FormatExecutorSchema,
  context: ExecutorContext
) {
  const packagePath = getPackagePath(context, options.packageList === true);

  const args = options.args ?? [];
  args.push(packagePath);

  executeGoCommand({
    tool: 'fmt',
    args,
  });
  return {
    success: true,
  };
}
