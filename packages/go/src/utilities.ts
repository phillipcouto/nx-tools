import { normalizePath } from '@nx/devkit';

/**
 * nameWithPath will return the name and path normalized to unix style paths
 * @param value
 * @returns
 */
export const nameWithPath = (value: string) => {
  const parsedValue = normalizePath(value).split('/');
  const name = parsedValue.pop();
  const path = parsedValue.join('/');
  return { name, path };
};

/*
 * Replaces all path slashes in value with hyphens
 */
export const normalizeProjectName = (value: string) =>
  value.replace(new RegExp('/', 'g'), '-');
