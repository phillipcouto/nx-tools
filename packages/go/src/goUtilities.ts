import { logger, Tree } from '@nx/devkit';
import * as stream from 'stream';
import * as readline from 'readline';
import * as child_process from 'child_process';
import { EOL } from 'os';
import { execSync } from 'child_process';
import { ExecutorContext } from 'nx/src/config/misc-interfaces';

const goVersionFromCommandRegex = /go(\d+\.\d+)/;
const workspaceDirectives = {
  go: /go\s(\d+\.\d+)/,
  toolchain: /toolchain\s(go\d+\.\d+\.\d+)/,
};

/**
 * Get the version of the go workspace from go.work file
 * @param tree
 * @returns
 */
export const getGoWorkspaceVersion = async (tree: Tree) => {
  const data = tree.read('go.work');
  if (data.length === 0) {
    throw new Error('go.work is empty');
  }
  const bufferStream = new stream.Readable();
  bufferStream.push(data);
  bufferStream.push(null);

  const rl = readline.createInterface({
    input: bufferStream,
  });
  for await (const line of rl) {
    const matches = line.match(workspaceDirectives.go);
    if (matches && matches[1]) {
      return matches[1];
    }
  }
  throw new Error(`unable to find 'go X.XX' in go.work: ${data.toString()}`);
};
/**
 * Get the version of go using the go command
 * @returns
 */
export const getGoVersionFromCommand = async () => {
  const { stdout } = child_process.exec('go version');
  const rl = readline.createInterface({
    input: stdout,
  });
  for await (const line of rl) {
    const matches = line.match(goVersionFromCommandRegex);
    if (matches && matches[1]) {
      return matches[1];
    }
  }
  throw new Error(`unable to extract go version from 'go version'`);
};
/**
 * Represents the go.work file after being parsed
 */
export interface GoWorkFile {
  go: string;
  toolchain?: string;
  use: string[];
  replace: string[];
}
/**
 * Parses the contents of a go.work file and returns an easy to use interface
 */
export const parseGoWorkFile = async (data: Buffer | string) => {
  if (!data || data.length === 0) {
    throw new Error('input is empty');
  }
  const bufferStream = new stream.Readable();
  bufferStream.push(data);
  bufferStream.push(null);

  const fileData: GoWorkFile = {
    go: '',
    use: [],
    replace: [],
  };

  const rl = readline.createInterface({
    input: bufferStream,
  });
  let multilineDirective: '' | 'use' | 'replace' = '';
  for await (const line of rl) {
    const goDirective = line.match(workspaceDirectives.go);
    if (goDirective) {
      if (fileData.go) {
        throw new Error(`invalid file, multiple 'go' directives`);
      }
      fileData.go = goDirective[1];
      continue;
    }
    const toolchainDirective = line.match(workspaceDirectives.toolchain);
    if (toolchainDirective) {
      if (fileData.toolchain) {
        throw new Error(`invalid file, multiple 'toolchain' directives`);
      }
      fileData.toolchain = toolchainDirective[1];
      continue;
    }
    if (line.startsWith(')')) {
      multilineDirective = '';
      continue;
    }
    switch (multilineDirective) {
      case 'use':
        fileData.use.push(line.trim());
        continue;
      case 'replace':
        fileData.replace.push(line.trim());
        continue;
    }
    if (line.startsWith('use')) {
      if (line.endsWith('(')) {
        multilineDirective = 'use';
        continue;
      }
      fileData.use.push(line.substring(3).trim());
      continue;
    }
    if (line.startsWith('replace')) {
      if (line.endsWith('(')) {
        multilineDirective = 'replace';
        continue;
      }
      fileData.replace.push(line.substring(7).trim());
    }
  }
  if (multilineDirective) {
    throw new Error(
      `invalid file, multiline directive '${multilineDirective}' not complete`
    );
  } else if (!fileData.go) {
    throw new Error('invalid file, go directive missing');
  }
  return fileData;
};
/**
 * Writes the GoWorkFile to an actual go.work file format
 */
export const writeGoWorkFile = (fileData: GoWorkFile) => {
  const parts: string[] = ['go ' + fileData.go];
  if (fileData.toolchain) {
    parts.push('toolchain ' + fileData.toolchain);
  }
  if (fileData.use.length > 1) {
    fileData.use.sort();
    const multiline = [
      'use (',
      fileData.use.map((value) => '\t' + value).join(EOL),
      ')',
    ].join(EOL);
    parts.push(multiline);
  } else if (fileData.use.length === 1) {
    parts.push('use ' + fileData.use[0]);
  }
  if (fileData.replace.length > 1) {
    fileData.replace.sort();
    const multiline = [
      'replace (',
      fileData.replace.map((value) => '\t' + value).join(EOL),
      ')',
    ].join(EOL);
    parts.push(multiline);
  } else if (fileData.replace.length === 1) {
    parts.push('replace ' + fileData.replace[0]);
  }
  return parts.join(EOL + EOL); // Use to end of lines so there is adequate space around the directives
};

/**
 * Input for running a go command
 */
export interface GoCommandInput {
  // The tool to use
  tool: 'build' | 'fmt' | 'test';
  // arguments to provide to the tool
  args: string[];
  // environment variables to set in addition to the ones from process.env
  env?: { [key: string]: string };
}

/**
 * executes a go command
 * @param input
 */
export const executeGoCommand = ({ tool, args, env }: GoCommandInput) => {
  const command = ['go', tool, ...args];
  const commandEnv = env ? { ...process.env, ...env } : process.env;
  const result = execSync(command.join(' '), { env: commandEnv });
  logger.info(result);
};
/**
 * Creates a packages argument for a go command
 * @param context
 * @param packageList
 */
export const getPackagePath = (
  context: ExecutorContext,
  packageList: boolean
) => {
  let packagePath = context.workspace.projects[context.projectName].root;
  if (!packagePath.startsWith('./')) {
    packagePath = './' + packagePath;
  }
  if (packageList) {
    packagePath += packagePath.endsWith('/') ? '...' : '/...';
  }
  return packagePath;
};
